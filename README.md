# Set a route53 record to your current global IP

that's all it does.

### Setup
This is a python3 app, so it would be good to have a python3-specific copy of pip installed. Once that's all set, install the requirements:

`pip3 install -r requirements.txt`

### Requirements

You:
* have an AWS account
* have a hosted zone of some kind
* have your creds sitting in ~/.aws/credentials or some other inferrable source as a `default` profile

You don't actually need awscli installed for this. It's entirely python/boto. You just need your account setup. 

### Running it

First off, there's no dry run. It runs and makes the record, fails, or figures out it doesn't need to do anything. 

Run it like so:

`python3 set_dns.py -u subdomain.domain.com`

Operations:
* get the current resolved IP for the url you used
* get your current global IP
* compare the two -- halt the process if they match
* strip the root domain from your url 
* list your hosted zones and seek a match for the root domain
* return the zone id of the matching hosted zone
* use that to create/update the A record matching the url you specified


 