from requests import get
import socket
import boto3
import argparse

"""
arguments!
"""
params = argparse.ArgumentParser()
params.add_argument('-u', '--url', help='the external url to update')
args = params.parse_args()


def compare_records(url):
  """
  Accepts a string var for the url, gets the current external record for it
  gets current global IP of the host system
  compares the two
  """
  try:
    current_dns_record = socket.gethostbyname(url)
  except socket.gaierror as e:
    print(e)
    current_dns_record = '0.0.0.0' # force a non-equating value so it keeps going. this is hacky
  system_external_ip = get('https://api.ipify.org').text
  if current_dns_record == system_external_ip:
    compare_result = 'match'
  else:
    compare_result = 'different'
  return compare_result, system_external_ip


def update_r53_record(hosted_zone_id, url, new_address):
  r53 = boto3.client('route53')
  response = r53.change_resource_record_sets(
    HostedZoneId=hosted_zone_id,
    ChangeBatch={
      'Changes': [
        {
          'Action': 'UPSERT',
          'ResourceRecordSet': {
            'Name': url,
            'Type': 'A',
            'TTL': 180,
            'ResourceRecords': [
              {
                'Value': new_address,
              },
            ],
          }
        },
      ]
    }
  )
  return response


def get_hosted_zone_id(base_zone_name):
  r53 = boto3.client('route53')
  zonelist = r53.list_hosted_zones_by_name()['HostedZones']
  zone_id_long = next((item['Id'] for item in zonelist if base_zone_name in item['Name']), 'none')
  if zone_id_long == 'none':
    zone_id = zone_id_long
  else:
    zone_id = zone_id_long.split('/')[-1]
  return zone_id


def evaluate_and_act(url):
  result, host_ip = compare_records(url)
  # below, splits the full record and grabs just the last two words
  # to form the base zone name/top level domain
  base_zone_name = str.join('.', ((url.split('.')[-2:])))
  if result == 'match':
    operation = "everything's current"
  else:
    zone_id = get_hosted_zone_id(base_zone_name)
    operation = update_r53_record(zone_id, url, host_ip)
  print(operation)
  return operation

if __name__ == "__main__":
  evaluate_and_act(args.url)
